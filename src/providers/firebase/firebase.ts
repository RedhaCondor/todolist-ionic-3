import {Injectable} from '@angular/core';
import {AngularFireDatabase} from "angularfire2/database";
import {GooglePlus} from "@ionic-native/google-plus";
import {TodoItem, TodoList, User} from "../../app/model/model";
import * as firebase from "firebase/app";
import {FileChooser} from "@ionic-native/file-chooser";


@Injectable()
export class FireProvider {

  test: number;

  constructor(private bd: AngularFireDatabase, private googlePlus: GooglePlus, private fileChooser: FileChooser) {
    console.log('Hello TododbProvider Provider');
    this.test = 0;
  }

  // get List from ID
  getList(uuidList: string) {
    return this.bd.object<TodoList>("App/Listes/" + uuidList + "/",).valueChanges();
  }

  // get all list of specific user
  getListesUser(uuidUser: String) {
    return this.bd.list<string>("App/Users/" + uuidUser + "/listes/", ref => ref.orderByChild('statut').equalTo(true)).valueChanges();
  }

  // get todoitem to list
  getTodoItem(listId: String) {
    return this.bd.list<TodoItem>('/App/Listes/' + listId + '/items').valueChanges();
  }

  // add todoList
  addTodolist(list: TodoList, uuidUser: String) {

    let ref = this.bd.list(`App/Listes`).push({});
    list.uuid = ref.key;
    list.items = [];
    list.count = 1;
    ref.set(list);
    this.bd.list("App/Users/" + uuidUser + "/listes/" + list.uuid).set('id', list.uuid);
    this.bd.list("App/Users/" + uuidUser + "/listes/" + list.uuid).set('statut', true);

  }

  // edit list
  editList(listId, name) {
    let ref = this.bd.object('App/Listes/' + listId + '/name');
    ref.set(name);
  }

  // edit item of specific list
  editItem(listId, itemId, name, desc) {

    let ref = this.bd.object('App/Listes/' + listId + '/items/' + itemId + '/name');
    ref.set(name);
    let ref2 = this.bd.object('App/Listes/' + listId + '/items/' + itemId + '/desc');
    ref2.set(desc);

  }

  // add item
  addItem(listId: String, item: TodoItem) {

    let ref = this.bd.list('App/Listes/' + listId + '/items').push({});
    item.uuid = ref.key;
    item.complete = false;
    ref.set(item);

    return ref.key
  }

  // end task
  changeStatItem(uuidList: string, uuidItem: string, val: boolean) {
    let ref = this.bd.object('App/Listes/' + uuidList + '/items/' + uuidItem + '/complete');
    ref.set(val);

  }

  editItemImage(uuidList: string, uuidItem: string, val: any) {
    let ref = this.bd.object('App/Listes/' + uuidList + '/items/' + uuidItem + '/image');
    ref.set(val);
  }

  // delete item
  deleteItem(uuidList: string, uuidItem: string) {
    this.bd.object('App/Listes/' + uuidList + '/items/' + uuidItem + '/').remove();
  }

  // delete list
  deleteList(idList: string) {
    let ref = this.bd.object('App/Listes/' + idList);
    ref.remove()

  }

  // delest list from user association
  deleteListUser(idList: string, idUser: String) {

    let ref = this.bd.object("App/Users/" + idUser + "/listes/" + idList);
    ref.remove();

    firebase.database().ref("App/Listes/" + idList + "/count").once("value").then(value => {

      this.test = value.val()

      let number = this.test - 1;

      this.bd.list("App/Listes/" + idList + "/").set("count", number)

      if (number == 0) {

        this.deleteList(idList)

      }

    })

  }

  // add user
  addUser(uuidUser: String) {

    this.bd.list(`App/Users/` + uuidUser);

  }


}
