import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {GooglePlus} from "@ionic-native/google-plus";
import {AngularFireDatabase} from "angularfire2/database";
import {TodoList} from "../../app/model/model";
import {FireProvider} from "../firebase/firebase";
import * as firebase from "firebase/app";

/*
  Generated class for the ShareProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ShareProvider {

  test : number;

  constructor(private bd: AngularFireDatabase, private googlePlus: GooglePlus , private service : FireProvider) {
    console.log('Hello ShareProvider Provider');
    this.test = 0 ;
  }

  // Share list
  partageList(idUser, uuidList: string) {

    this.bd.list("App/Users/" + idUser + "/listes/" + uuidList).set("id", uuidList);
    this.bd.list("App/Users/" + idUser + "/listes/" + uuidList).set("statut", false);

  }

  // Shared list with qr code
  partageListQrCode(idUser, uuidList: string) {

    this.bd.list("App/Users/" + idUser + "/listes/" + uuidList).set("id", uuidList);
    this.bd.list("App/Users/" + idUser + "/listes/" + uuidList).set("statut", true);
    this.editCount(uuidList)

  }


  // Send list to other user
  sendList(list: TodoList, uuidUser: String) {

    let ref = this.bd.list(`App/Listes`).push({});
    list.uuid = ref.key;
    ref.set(list);
    this.bd.list("App/Users/" + uuidUser + "/listes/" + list.uuid).set("id", list.uuid);
    this.bd.list("App/Users/" + uuidUser + "/listes/" + list.uuid).set("statut", false);

  }


  // Display shared list
  getSharedListes(uuidUser: String) {

    return this.bd.list<string>("App/Users/" + uuidUser + "/listes/", ref => ref.orderByChild('statut').equalTo(false)).valueChanges();

  }

  // Confirme shared list
  confirmeList(idUser, uuidList) {

    this.bd.list("App/Users/" + idUser + "/listes/" + uuidList).set("statut", true);
    this.editCount(uuidList)

  }

  rejectList(idUser, idList) {
    let ref = this.bd.object("App/Users/" + idUser + "/listes/" + idList);
    ref.remove()
  }

  // Increment Count List
  editCount(uuidList) {

    firebase.database().ref("App/Listes/" + uuidList + "/count").once("value").then(value => {

      this.test = value.val()

      let number = this.test+1;

      this.bd.list("App/Listes/" + uuidList + "/").set("count",number)

    })

   // this.bd.object<TodoList>("App/Listes/" + uuidList + "/").valueChanges().subscribe(value => {
   //  this.test = value.count;
   // });

  }



}
