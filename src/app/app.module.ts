import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler, Badge} from 'ionic-angular';
import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {ListItemPage} from "../pages/list-item/list-item";
import {DetailItemPage} from "../pages/detail-item/detail-item";
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth'
import {LoginPage} from "../pages/login/login";
import {HomePageModule} from "../pages/home/home.module";
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { GooglePlus } from '@ionic-native/google-plus';
import {SpeechRecognition} from "@ionic-native/speech-recognition";
import {SharedListPage} from "../pages/shared-list/shared-list";
import { ShareProvider } from '../providers/share/share';
import { FireProvider} from '../providers/firebase/firebase';
import { HttpClientModule } from '@angular/common/http';
import { Camera } from '@ionic-native/camera';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import {FileChooser} from "@ionic-native/file-chooser";
import {FilePath} from "@ionic-native/file-path";

export const firebaseConfig = {
  apiKey: "AIzaSyD9sMm2aaZR4iLzr2n9T3uJTSZfNY8yupw",
  authDomain: "authentificationexemple.firebaseapp.com",
  databaseURL: "https://authentificationexemple.firebaseio.com",
  projectId: "authentificationexemple",
  storageBucket: "authentificationexemple.appspot.com",
  messagingSenderId: "78835609960"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    ListItemPage,
    LoginPage,
    SharedListPage,
    TabsPage
  ],
  imports: [
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    HomePageModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    SharedListPage,
    LoginPage,
    ListItemPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GooglePlus,
    SpeechRecognition,
    Badge,
    BarcodeScanner,
    FileChooser,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FireProvider,
    Camera,
    ShareProvider,
  ]
})
export class AppModule {}
