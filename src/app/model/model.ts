export interface TodoList {
  uuid : string,
  name : string,
  items : TodoItem[]
  count? : number,
}

export interface TodoItem {
  uuid? : string,
  name : string,
  desc? : string,
  image?:string
  complete : boolean
}

export interface User {
  email: string;
  password: string;
}
