import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams, ViewController} from 'ionic-angular';
import {TodoItem} from "../../app/model/model";
import {FireProvider} from "../../providers/firebase/firebase";
import {SpeechRecognition} from "@ionic-native/speech-recognition";
import {Camera,CameraOptions} from "@ionic-native/camera";
import firebase from 'firebase';
import {FileChooser} from "@ionic-native/file-chooser";
import {HomePage} from "../home/home";
import {collectExternalReferences} from "@angular/compiler";
import {AngularFireAuth} from "angularfire2/auth";


/**
 * Generated class for the AddItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 S Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-item',
  templateUrl: 'add-item.html',
})
export class AddItemPage {

  title: string;
  description: string;
  complete: boolean;
  listId: string;
  nativepath: any = '';
  idItem : any;
  image : string;
  userId: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
              private service: FireProvider, private speechRecognition: SpeechRecognition ,
              private camera : Camera, public loadingCtrl: LoadingController,public afAuth: AngularFireAuth,
              private fileChooser: FileChooser , public alertCtrl: AlertController) {

    this.listId = this.navParams.get('listId');
    this.userId = this.afAuth.auth.currentUser.uid;


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddItemPage');
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

  // Vocal regognition
  async vocalRecognition(action) {

    const hasPermission = await this.speechRecognition.hasPermission();
    if (!hasPermission) {
      await this.speechRecognition.requestPermission();
    }
    this.speechRecognition.startListening().subscribe(async terms => {

        let data = terms[0];

        if (action == 0) {

          this.title = data

        } else {

          this.description = data

        }

      }
    ), error => console.log(error);

  }

  // Save new item
  saveItem() {

    let item = {
      name: this.title,
      desc: this.description,
      complete: false,
      image : ''
    };

    // if url exist
    if (this.image!=null){

      item.image = this.image

    }

    // return id item for push image with id name of item
    this.idItem = this.service.addItem(this.listId, item);

    // if user choice fileUpload
    if(this.nativepath!=""){

      this.uploadImage(this.idItem)

    }

    this.viewCtrl.dismiss();

  }

  // Add url file
  addImage() {
    this.fileChooser.open().then(url => {
      (<any>window).FilePath.resolveNativePath(url, result => {
        this.nativepath = result;
      });
    });
  }

  // Upload File on firebase
  uploadImage(item) {
    (<any>window).resolveLocalFileSystemURL(this.nativepath, res => {
      res.file(resFile => {
        var reader = new FileReader();
        reader.readAsArrayBuffer(resFile);
        reader.onloadend = (evt: any) => {
          var imgBlob = new Blob([evt.target.result], { type: "image/jpeg" });
          var imageStore = firebase.storage().ref('images/'+this.userId+'/'+item);
          imageStore
            .put(imgBlob)
            .then(res => {
              this.image = res.downloadURL;
              this.service.editItemImage(this.listId,this.idItem,this.image)
            })
            .catch(err => {
              alert("Upload Failed" + err);
            });
        };
      });
    });
  }

// Use Camera for take picture
 takePicture() {

    this.nativepath = "";

    const options: CameraOptions = {
      quality: 100,
      targetHeight: 300,
      targetWidth: 300,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    const result = this.camera.getPicture(options);

    result.then(succes=>{
      const selfieRef = firebase.storage().ref('images/itemPicture.png');
      selfieRef.putString(succes, 'base64', {contentType: 'image/png'}).then(savedProfilePicture => {
        this.image = savedProfilePicture.downloadURL
      });

    }, error => {
      // Log an error to the console if something goes wrong.
      alert(JSON.stringify(error));
    });


  }

  // Display Dialog if user want show list of action
  menuDialog() {
    let actionSheet = this.alertCtrl.create({
      title: 'Menu liste',
      buttons: [
        {
          text: 'Selectionner une images',
          handler: () => {
            console.log('Archive clicked');
            this.addImage()
          }
        },
        {
          text: 'Prendre une photo',
          handler: () => {

            this.takePicture()

          }
        }

      ]
    });
    actionSheet.present();

  }


}
