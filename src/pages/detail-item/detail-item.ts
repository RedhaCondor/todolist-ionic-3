import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {ListItemPage} from "../list-item/list-item";
import {FireProvider} from "../../providers/firebase/firebase";
import {AngularFireAuth} from "angularfire2/auth";

/**
 * Generated class for the DetailItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-item',
  templateUrl: 'detail-item.html',
})
export class DetailItemPage {

  uuidItem : string ;
  uuidlist : string ;
  picture : string;
  userId : string;

  item = {

    uuid : '',
    name : '',
    desc : '',
    image :'',
    complete : false

  }

  constructor(public navCtrl: NavController,public viewCtrl : ViewController,public afAuth: AngularFireAuth,
              public alertCtrl: AlertController, public navParams: NavParams , public service :FireProvider) {

  }

  ionViewDidLoad() {

    this.uuidItem = this.navParams.get('itemId');
    this.uuidlist = this.navParams.get('listId');

    this.item = {

      uuid : this.navParams.get('itemId'),
      name :this.navParams.get('itemName'),
      desc : this.navParams.get('itemDesc'),
      image : this.navParams.get('itemImage'),
      complete : this.navParams.get('itemCompleted'),

    }

    this.picture = this.item.image;

  }

  // Task complete
  endTask(){

    this.item.complete = true;
    // end task
    this.service.changeStatItem(this.uuidlist,this.uuidItem,this.item.complete)

  }

  public closeModal(){
    this.viewCtrl.dismiss();
  }


   // Delete Item
  suppItem(){
    this.service.deleteItem(this.uuidlist,this.uuidItem)
    this.closeModal()
  }

  // Edit Item
  editItem() {

    let prompt = this.alertCtrl.create({
      title: 'Edit Item',
      message: "Modifier l'item",
      inputs: [
        {
          name: 'name',
          placeholder: 'Name',
          value : this.item.name
        },
        {
          name: 'desc',
          placeholder: 'Description',
          value: this.item.desc
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked')

            // Modifier un item
            this.service.editItem(this.uuidlist,this.uuidItem,data.name,data.desc);


            this.closeModal()

          }
        }
      ]
    });
    prompt.present();

  }


}
