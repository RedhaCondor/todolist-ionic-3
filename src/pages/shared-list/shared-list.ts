import { Component } from '@angular/core';
import {AlertController, Badge, IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {Observable} from "rxjs/Observable";
import {AngularFireAuth} from "angularfire2/auth";
import {SpeechRecognition} from "@ionic-native/speech-recognition";
import {GooglePlus} from "@ionic-native/google-plus";
import {ShareProvider} from "../../providers/share/share";
import {FireProvider} from "../../providers/firebase/firebase";
import {TodoList} from "../../app/model/model";


@IonicPage()
@Component({
  selector: 'page-shared-list',
  templateUrl: 'shared-list.html',
})
export class SharedListPage {

  listsUsers: Observable<any[]>;
  lists: Observable<TodoList>;
  userId: String;
  todoList: TodoList[] = [];
  number : number = 0;

  constructor(public navCtrl: NavController, public afAuth: AngularFireAuth,
              public navParams: NavParams, public alertCtrl: AlertController,
              private service: FireProvider, private googlePlus: GooglePlus,
              private speechRecognition: SpeechRecognition,
              private share : ShareProvider,public viewCtrl: ViewController) {

    // get current userId
    this.userId = this.afAuth.auth.currentUser.email;

    this.userId = this.convertEmail(this.userId)

    this.listsUsers = share.getSharedListes(this.userId);

    this.listsUsers.subscribe(value => {

      this.todoList = [];

      for (let val of value) {

        this.lists = service.getList(val.id);

        this.lists.subscribe(value => {

          // add list to todoList
          if (!this.todoList.find(value2 => value2.uuid == value.uuid)) {

            this.todoList.push(value)
          }

        })

      }
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SharedListPage');
  }

  // Convert Email adress to ID
  convertEmail(value){

    value = value.replace('@','')
    value = value.replace(/\./g, ',')

    return value

  }

  // Confirme shared list
  confirmeList(item: TodoList) {

    this.share.confirmeList(this.userId,item.uuid)
    this.closeModal();

  }

  // Decline shared list
  declineList(item: TodoList) {

    this.share.rejectList(this.userId,item.uuid)
    this.closeModal();

  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

}
