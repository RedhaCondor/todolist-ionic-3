import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {User} from "../../app/model/model";
import {GooglePlus} from "@ionic-native/google-plus";
import {FireProvider} from "../../providers/firebase/firebase";
import {AngularFireAuth} from "angularfire2/auth";
import firebase from "@firebase/app";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {} as User;
  userId: any;


  constructor(private afAuth: AngularFireAuth,private platform: Platform,
              public navCtrl: NavController, public navParams: NavParams , private googlePlus: GooglePlus,   private service: FireProvider ) {
  }

  async login(user: User) {
    try {
      const result = await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
      if (result) {
        this.navCtrl.push('HomePage');
      }

    }
    catch (e) {
      console.error(e);
    }
  }

  async loginGoogle() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then( user => {

      this.navCtrl.setRoot('HomePage');

    }).catch(err => {

      alert(err.message)

    });

  }


  async nativeGoogleLogin() : Promise<void> {
    try {

      const gplusUser = await this.googlePlus.login({
        'webClientId': '78835609960-2dccr730skiaa4kesijbshens6locijl.apps.googleusercontent.com',
        'offline': true,
      },)

      return await this.afAuth.auth.signInWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken))

    } catch(err) {
      console.log(err)
    }
  }

  async googleLogin() {
    if (this.platform.is('cordova')) {
      this.nativeGoogleLogin().then(res=>{
        this.navCtrl.setRoot('HomePage');
      });

    } else {
      this.loginGoogle()
    }
  }


  async register(user: User) {
    try {
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(
        user.email,
        user.password
      );
      if (result) {
        this.navCtrl.push('HomePage');
      }
    } catch (e) {
      console.error(e);
    }
  }

}

