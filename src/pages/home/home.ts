import {Component} from '@angular/core';
import {AlertController, Badge, IonicPage, ModalController, NavController, NavParams, Toast} from 'ionic-angular';
import {Observable} from "rxjs/Observable";
import {ListItemPage} from "../list-item/list-item";
import {AngularFireAuth} from "angularfire2/auth";
import {LoginPage} from "../login/login";
import {GooglePlus} from "@ionic-native/google-plus";
import {SpeechRecognition} from "@ionic-native/speech-recognition";
import {SharedListPage} from "../shared-list/shared-list";
import {ShareProvider} from "../../providers/share/share";
import {TodoItem, TodoList} from "../../app/model/model";
import {FireProvider} from "../../providers/firebase/firebase";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  listsUser: Observable<any[]>;
  list: Observable<TodoList>;
  userId: String;
  todoList: TodoList[] = [];
  user: any
  todolist: TodoList;
  number: number = 0

  constructor(public navCtrl: NavController, public afAuth: AngularFireAuth,
              public navParams: NavParams, public alertCtrl: AlertController,
              private service: FireProvider, private googlePlus: GooglePlus,
              private speechRecognition: SpeechRecognition, public modalCtrl: ModalController,
              private share: ShareProvider, private barcodeScanner: BarcodeScanner) {


    // get current userId
    this.userId = this.afAuth.auth.currentUser.email;

    //ocnvert email to id
    this.userId = this.convertEmail(this.userId)

    // add user
    service.addUser(this.userId);

    // init badge
    this.initBadge();

    // get lists of current user
    this.listsUser = service.getListesUser(this.userId);

    this.listsUser.subscribe(value => {

      for (let val of value) {

        this.list = service.getList(val.id);
        this.list.subscribe(value => {

          // check if list exist in local
          const existingList = this.todoList.find(value2 => value2.uuid == value.uuid)

          // add list to todoList
          if (!existingList) {
            this.todoList.push(value)
          } else {
            existingList.name = value.name
          }


        })
      }

    })

  }

  // Init number of sharedList in icone ( badge)
  initBadge() {

    this.share.getSharedListes(this.userId).subscribe(value => {

      this.number = value.length

    })

  }

  // When user select item
  itemSelected(item) {

    this.navCtrl.push(ListItemPage, {
      listId: item.uuid, listName: item.name
    });

  }

  // Convert email adresse to ID
  convertEmail(email) {

    email = email.replace('@', '')
    email = email.replace(/\./g, ',')

    return email

  }

  // Logout
  logoutUser(): Promise<void> {

    return this.afAuth.auth.signOut().then(res => {
      this.navCtrl.setRoot(LoginPage);
    });

  }

  // Vocal Recognition
  async vocalRecognition(data) {

    const hasPermission = await this.speechRecognition.hasPermission();
    if (!hasPermission) {
      await this.speechRecognition.requestPermission();
    }

    this.speechRecognition.startListening().subscribe(async terms => {

        data.name = terms[0];
        this.service.addTodolist(data, this.userId);

      }
    ), error => console.log(error);

  }

  // Share list with other user
  async shareListDialog(item, action) {

    let prompt = this.alertCtrl.create({
      title: 'Partager cette liste',
      message: "",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email'
        },
      ],
      buttons: [

        {
          text: 'Annuler',
          handler: data => {

          },

        },
        {
          text: 'Confirmer',
          handler: data => {

            if (action == 0) {

              var mailId = this.convertEmail(data.email);

              // check if user already exist in database
              this.share.partageList(mailId, item.uuid);

            } else {

              var mailId = this.convertEmail(data.email);
              this.todolist = item;

              this.share.sendList(this.todolist, mailId)

            }

          },

        }
      ]
    });
    prompt.present();

  }


  // Display Dialog if user want edit list
  editListDialog(item) {

    let prompt = this.alertCtrl.create({
      title: 'Modifier la liste',
      message: "Entrez le nom de votre liste",
      inputs: [
        {
          name: 'name',
          placeholder: item.name,
          value: item.name
        },
      ],
      buttons: [
        {
          text: 'Save',
          handler: data => {

            console.log('Saved clicked');
            this.service.editList(item.uuid, data.name);

            // refresh page
            this.navCtrl.setRoot(HomePage)

          }
        },

        {
          text: 'Voice',
          handler: data => {
            this.vocalRecognition(data);
          }
        },

      ]
    });

    prompt.present();


  }

  // Display Dialog if user want add list
  addListDialog() {
    let prompt = this.alertCtrl.create({
      title: 'Ajout liste',
      message: "Entrez le nom de votre liste",
      inputs: [
        {
          name: 'name',
          placeholder: 'Name'
        },
      ],
      buttons: [
        {
          text: 'Ajouter',
          handler: data => {

            if (data.name != '') {
              this.service.addTodolist(data, this.userId)
            } else {
              alert("Merci de renseigner un nom de liste")
            }
            console.log('Saved clicked');


          }
        },
        {
          text: 'Voice',
          handler: data => {
            this.vocalRecognition(data);
          }
        },


      ]
    });
    prompt.present();

  }

  // Display Dialog if user want show list of action
  menuDialog(item) {
    let actionSheet = this.alertCtrl.create({
      title: 'Menu liste',
      buttons: [{
        text: 'Editer',
        handler: () => {
          this.editListDialog(item)
        }
      }
        , {
          text: 'Partager',
          handler: () => {
            console.log('Archive clicked');
            this.shareListDialog(item, 0);

          }
        },
        {
          text: 'QR CODE',
          handler: () => {
            console.log('Archive clicked');
            this.displayQrCode(item.uuid)

          }
        },
        {
          text: 'Envoyer',
          handler: () => {
            console.log('Archive clicked');
            this.shareListDialog(item, 1);

          }
        },
        {
          text: 'Supprimer',
          handler: () => {
            this.service.deleteListUser(item.uuid, this.userId)

            // refresh page
            this.navCtrl.setRoot(HomePage)
          }
        }

      ]
    });
    actionSheet.present();

  }

  // Display qr code
  displayQrCode(uuid) {

    this.barcodeScanner.encode("TEXT_TYPE", uuid)

  }

  // Scan Qr code
  scanId() {

    this.barcodeScanner.scan().then(barcodeData => {

      this.share.partageListQrCode(this.userId, barcodeData.text)

    }).catch(err => {
      console.log('Error', err);
    });

  }

  // Display shared List
  showSharedList() {
    this.modalCtrl.create(SharedListPage).present();

    // this.navCtrl.push(SharedListPage)

  }

}
