import {Component} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, ViewController} from 'ionic-angular';
import {DetailItemPage} from "../detail-item/detail-item";
import {observable} from "rxjs/symbol/observable";
import {AngularFireList} from "angularfire2/database";
import {Observable} from "rxjs/Observable";
import * as firebase from "firebase/app";
import {SpeechRecognition} from "@ionic-native/speech-recognition";
import {FireProvider} from "../../providers/firebase/firebase";
import {TodoItem} from "../../app/model/model";
import {AddItemPage} from "../add-item/add-item";

/**
 * Generated class for the ListItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-list-item',
  templateUrl: 'list-item.html',
})

export class ListItemPage {

  listId: String;
  listName:String;
  item : TodoItem;
  listItems: Observable<TodoItem[]>


  constructor(public navCtrl: NavController, public alertCtrl: AlertController,
              public navParams: NavParams, private service: FireProvider,
              private speechRecognition: SpeechRecognition, public modalCtrl : ModalController) {

  }

  ionViewDidLoad() {

    // On récoupere id de la list
    this.listId= this.navParams.get('listId');
    this.listName= this.navParams.get('listName');

    // on récoupére les items de la liste
    this.listItems = this.service.getTodoItem(this.listId)

  }

  addItemModal(){

    this.modalCtrl.create('AddItemPage',{listId:this.listId}).present();

  }

  addItem() {

    let prompt = this.alertCtrl.create({
      title: 'Ajouter Item',
      message: "",
      inputs: [
        {
          name: 'name',
          placeholder: 'Name'
        },
        {
          name: 'desc',
          placeholder: 'Description'
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked');

            this.item = data
            this.item.complete = false;
            this.service.addItem(this.listId,data)

          }
        },


      ]
    });
    prompt.present();

  }

  showDetail(item) {

    this.modalCtrl.create('DetailItemPage',{
      listId: this.listId, itemName: item.name, itemDesc: item.desc, itemCompleted: item.complete,itemId:item.uuid,itemImage:item.image}).present();

  }

}
